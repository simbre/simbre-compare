# Simbre Compare #

Enables comparing of sound-files. Result of the comparison will be a distance value.
This distance value can than be used finding alike sounds in a sound-space.

## Create docker image

```
docker build --no-cache -t simbre-compare .
```

Extra parameters for docker run

```
--user UID:GID \
-v HOME/.config:/.config \
-v HOME/.cache:/.cache
```

E.g.

```
--user 1000:1000 \
-v /home/wwagner4/.config:/.config \
-v /home/wwagner4/.cache:/.cache
```

import tryout as to


def test_endpoints():
    _envelope = [1.0, 1.0, 0.5]
    _length = 4

    xs, ys = to.envelope_to_endpoints(_length, _envelope)

    x_str = ", ".join([f"{f:.3f}" for f in xs])
    x_exp_str = ", ".join([f"{f:.3f}" for f in [0, 2, 4]])
    y_str = ", ".join([f"{f:.3f}" for f in ys])
    y_exp_str = ", ".join([f"{f:.3f}" for f in [1, 1, 0.5]])
    assert x_str == x_exp_str
    assert y_str == y_exp_str


def test_refine_envelope_a():
    # 0  0.5   1   0.75 0.5  0.25   0
    # 0   |    1    |    2    |     3

    _len = 3
    _env_rate = 2
    _re = [1, 0.5]
    _exp = [0, 0.5, 1, 0.75, 0.5, 0.25, 0]

    refined = to.refine_envelope(_len, _env_rate, _re)

    exp_str = ", ".join([f"{f:.3f}" for f in _exp])
    refined_str = ", ".join([f"{f:.3f}" for f in refined])
    assert exp_str == refined_str


def test_refine_envelope_b():
    _len = 5
    _env_rate = 2
    _re = [1, 0.5]
    _exp = [0, 0.3, 0.6, 0.9, 0.9, 0.75, 0.6, 0.45, 0.3, 0.15, 0]

    refined = to.refine_envelope(_len, _env_rate, _re)

    exp_str = ", ".join([f"{f:.3f}" for f in _exp])
    refined_str = ", ".join([f"{f:.3f}" for f in refined])
    assert exp_str == refined_str

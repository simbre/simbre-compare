FROM python:3.9-slim

ARG DEBIAN_FRONTEND="noninteractive"

COPY Pipfile* ./

RUN python -m pip install --upgrade pip
RUN pip install pipenv
RUN pipenv install --system --deploy --dev


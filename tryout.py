import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io.wavfile
from numpy.fft import irfft
from scipy.fft import rfft
from scipy.fftpack import rfftfreq
from scipy.interpolate import interp1d

import helpers as hlp


def compare_ndarrays():
    a = np.array([1.3, 2.5, 2.12])
    b = np.array([1.33, 2.5, 2.11])
    diff = a - b
    print(diff)
    s_diff = np.power(diff, 2)
    print(s_diff)
    diff_sum = s_diff.sum()
    print(diff_sum)


def compare_two_files():
    sdl = hlp.read_wav(hlp.in_dir / "t1l.wav")
    sdr = hlp.read_wav(hlp.in_dir / "t1r.wav")

    print("-- describe left --------------")
    print(pd.Series(sdl.data).describe())
    print("-- describe right --------------")
    print(pd.Series(sdr.data).describe())

    diff = hlp.compare_snd_data(sdl, sdr)
    print("Diff:", diff, type(diff))


def plot_two_files():
    plot_file = hlp.target_dir / "t1.svg"

    sdl = hlp.read_wav(hlp.in_dir / "t1l.wav")
    sdr = hlp.read_wav(hlp.in_dir / "t1r.wav")

    print("-- describe left --------------")
    print(pd.Series(sdl.data).describe())
    print("-- describe right --------------")
    print(pd.Series(sdr.data).describe())

    time_len = 100
    start = 500

    times = range(time_len)
    al = sdl.data[start:start + time_len]
    ar = sdr.data[start:start + time_len]
    plt.plot(times, al, label="left")
    plt.plot(times, ar, label="right")
    plt.legend()
    plt.xlabel("time relative")
    plt.ylabel("amplitude")
    plt.savefig(plot_file)
    print(f"wrote plot to: {plot_file}")


def linear_interpolation():
    x = [0.00, 0.20, 0.40, 0.60, 0.80, 1.00, 1.20, 1.40, 1.60, 1.80, 2.00]
    y = [0.0, 0.1, 0.7, 0.66, 0.57, 0.41, 0.3, 0.2, 0.1, 0.01, 0.0]
    f = interp1d(x, y)

    out_file = hlp.target_dir / "lin.svg"

    plt.plot(x, y, '-ob')
    plt.plot(1.5, f(1.5), 'ro')
    plt.plot(1.7, f(1.7), 'ro')
    plt.plot(1.9, f(1.9), 'ro')
    plt.plot(0.5, f(0.5), 'ro')
    plt.title('Linear Interpolation')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig(out_file)
    print("Wrote linear interpolation example to", out_file)


def linear_interpolation_dev():
    duration = 2  # seconds
    step_count = 13

    eps = 0.00001
    step_duration = float(duration) / step_count
    xs = np.arange(0, duration + eps, step_duration)
    xss = ["{:.2f}".format(x) for x in xs]
    x_values = ", ".join(list(xss))

    yss = ["    " for _ in xs]
    y_values = ", ".join(list(yss))

    print(f"x = [{x_values}]")
    print(f"y = [{y_values}]")


def plot_phase():
    phi = np.random.rand(1) * 2 * np.pi
    print("-- rs", phi)
    xs = np.arange(0, 10, 0.1)
    ys = np.sin(xs + phi)
    plt.plot(xs, ys, '-')
    file = hlp.target_dir / "phase.svg"
    plt.savefig(file)
    print("plot phase to", file)


def read_write_sound():
    print("-- read write")
    wav_file = hlp.in_dir / "flute1.wav"
    snd_data = hlp.read_wav(wav_file)
    print(f"-- read {wav_file}")
    print(snd_data.data.dtype)

    def plot(name: str, data: np.ndarray, freqs: np.ndarray):
        plt.clf()
        plt.plot(freqs, data, "-")
        plt_file = hlp.target_dir / f"plt-{name}.svg"
        plt.xlabel("freq")
        plt.savefig(plt_file)
        print(f"-- wrote {plt_file}")

    def plot_range(name: str, data: np.ndarray, freqs: np.ndarray, range_start: int, range_end: int):
        plt.clf()
        plt.plot(freqs[range_start:range_end], data[range_start:range_end], "-")
        plt_file = hlp.target_dir / f"plt-{name}-{range_start}-{range_end}.svg"
        plt.xlabel("freq")
        plt.savefig(plt_file)
        print(f"-- wrote {plt_file}")

    def write_wav(name: str, signal: np.ndarray, ):
        print(signal.dtype)
        signal_norm = signal.astype(snd_data.data.dtype)
        print(signal_norm.dtype)
        fn = hlp.target_dir / f"flute1-{name}-norm.wav"
        scipy.io.wavfile.write(filename=fn, data=signal_norm, rate=snd_data.sample_rate)
        print("wrote sound to", fn)

    ampl_phase = np.fft.fft(snd_data.data)
    # ampl_phase = ampl_phase[range(int(len(snd_data.data) / 2))]  # Exclude sampling frequency
    ampl = np.abs(ampl_phase)
    _freqs = np.fft.fftfreq(len(snd_data.data), 1.0 / snd_data.sample_rate)
    print(_freqs)
    plot("ampl", ampl, _freqs)
    plot_range("ampl-range", ampl, _freqs, 0, 5000)

    ampl1 = ampl.copy()
    ampl1[ampl1 > 0.1e8] = 0.1e8
    plot("ampl1", ampl1, _freqs)

    phase = np.angle(ampl_phase)
    plot("phase", phase, _freqs)

    filtered = ampl_phase.copy()
    filtered[ampl < 0.01e7] = 0 + 0j

    real = np.real(np.fft.ifft(ampl_phase))
    real_filtered = np.real(np.fft.ifft(np.fft.ifft(filtered)))

    write_wav("real", real)
    write_wav("real-filtered", real_filtered)


def filter_array():
    a1 = np.array([0.1, 0.5, 0.7, 0.2])
    a1[a1 <= 0.5] = 0
    print(a1)


def tutorial_real_python():
    duration = 5
    sample_rate = hlp.CommonSampleRates.DEFAULT

    def create_sine(freq: float, gain: float = 1.0):
        x = np.linspace(0, duration, sample_rate * duration, endpoint=True)
        times = x * freq
        y = gain * np.sin((2 * np.pi) * times)
        return x, y

    _, y1 = create_sine(1000)
    _, y2 = create_sine(3000, gain=0.5)

    mixed_tone = y1 + y2

    tone1 = hlp.normalize_amplitude(mixed_tone)

    sd = hlp.SndData(hlp.CommonSampleRates.DEFAULT, tone1)
    wav_file = hlp.target_dir / "tone1.wav"
    hlp.write_wav(wav_file, sd)
    print("wrote to wavfile", wav_file)

    plt.plot(tone1[0:1000])
    plt.xlabel("time")
    plt.ylabel("amplitude")

    plt_file = hlp.target_dir / "tone1.svg"
    plt.savefig(plt_file)
    print("wrote to plot file", plt_file)

    yf = hlp.normalize_amplitude(np.abs(rfft(tone1)))
    n = sample_rate * duration
    xf = rfftfreq(n, 1 / sample_rate)

    plt.clf()
    plt.plot(xf[0:25000], yf[0:25000])
    plt.xlabel("frequency")
    plt.ylabel("amplitude")

    plt_file = hlp.target_dir / "tone1-spectrum.svg"
    plt.savefig(plt_file)
    print("wrote to plot file", plt_file)

    new_sig = irfft(yf)

    plt.clf()
    plt.plot(new_sig[:1000])
    plt.xlabel("time")
    plt.ylabel("amplitude")

    plt_file = hlp.target_dir / "tone1-revert.svg"
    plt.savefig(plt_file)
    print("wrote to plot file", plt_file)


def main():
    # tutorial_real_python
    # filter_array()
    read_write_sound()
    # plot_phase()
    # linear_interpolation()
    # linear_interpolation_dev()
    # compare_ndarrays()
    # compare_two_files()
    # plot_two_files()


if __name__ == '__main__':
    main()

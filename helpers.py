from dataclasses import dataclass
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors as mcolors
from scipy.io import wavfile

in_dir = Path(__file__).parent / "wavfile"

target_dir = Path(__file__).parent / "target"
if not target_dir.exists():
    target_dir.mkdir()


def cmap_rgb_values(color_map_name, colors_count):
    color_steps = 1.0 / (colors_count - 1.0)
    color_values = np.arange(0, 1.0 + 0.000001, color_steps)
    return [mcolors.to_rgba(x) for x in plt.get_cmap(color_map_name)(color_values)]


@dataclass
class CommonSampleRates:
    DEFAULT = 48000
    _48000 = 48000


@dataclass
class SndData:
    sample_rate: int
    data: np.ndarray

    def duration(self):
        return len(self.data) / self.sample_rate


def read_wav(wav_file: Path) -> SndData:
    sr, data = wavfile.read(wav_file)
    return SndData(sample_rate=sr, data=data)


def write_wav(wav_file: Path, snd_data: SndData):
    wavfile.write(filename=wav_file, data=snd_data.data, rate=snd_data.sample_rate)


def compare_snd_data(sd1: SndData, sd2: SndData) -> float:
    if sd1.sample_rate != sd2.sample_rate:
        raise AttributeError(
            f"For comparing sound data sample rates must be the same. sd1:{sd1.sample_rate} sd2{sd2.sample_rate} ")
    if len(sd1.data) != len(sd2.data):
        raise AttributeError(
            f"For comparing sound data length must be the same. sd1:{len(sd1.data)} sd2{len(sd2.data)} ")
    if sd1.data.dtype != sd2.data.dtype:
        raise AttributeError(
            f"For comparing sound data dtype must be the same. sd1:{sd1.data.dtype} sd2{sd2.data.dtype} ")
    data_diff = sd1.data - sd2.data
    return np.power(data_diff.astype(float), 2).sum()


def normalize_amplitude(snd: np.ndarray) -> np.ndarray:
    sm = snd.max(initial=np.finfo(snd.dtype).min)
    return np.int16((snd / sm) * 32767)
